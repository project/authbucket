<?php

/**
 * @file
 * Enables modules and site configuration for a authbucket site installation.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\features\ConfigurationItem;
use Drupal\features\FeaturesManagerInterface;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Implements hook_install_tasks().
 */
function authbucket_install_tasks($install_state) {
  return [
    'authbucket_batch' => [
      'type' => 'batch',
    ],
  ];
}

/**
 * Installation step callback.
 */
function authbucket_batch(&$install_state) {
  return [
    'title' => t('Installing @drupal', ['@drupal' => drupal_install_profile_distribution_name()]),
    'error_message' => t('The installation has encountered an error.'),
    'operations' => [
      ['_authbucket_configure_modules', []],
      ['_authbucket_configure_cleanup', []],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function authbucket_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Clear drupal message queue for non-errors.
  drupal_get_messages(NULL, TRUE);

  // Site information form.
  $form['site_information']['#weight'] = -20;
  $form['site_information']['site_name']['#default_value'] = \Drupal::request()->server->get('SERVER_NAME');
  $form['site_information']['site_mail']['#default_value'] = 'admin@example.com';

  // Administrator account form.
  $form['admin_account']['#weight'] = -19;
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'admin@example.com';

  // Power user account form.
  $form['webmaster_account'] = [
    '#type' => 'fieldgroup',
    '#title' => t('Site power user account'),
    '#collapsible' => FALSE,
  ];

  $form['webmaster_account']['#weight'] = -18;
  $form['webmaster_account']['webmaster_account']['#tree'] = TRUE;
  $form['webmaster_account']['webmaster_account']['name'] = [
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => 'webmaster',
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => t("Several special characters are allowed, including space, period (.), hyphen (-), apostrophe ('), underscore (_), and the @ sign."),
    '#required' => TRUE,
    '#attributes' => ['class' => ['username']],
  ];

  $form['webmaster_account']['webmaster_account']['mail'] = [
    '#type' => 'email',
    '#title' => t('Email address'),
    '#default_value' => 'webmaster@example.com',
    '#required' => TRUE,
  ];

  // Just alter the weight.
  $form['regional_settings']['#weight'] = -17;
  $form['update_notifications']['#weight'] = -16;
  $form['actions']['#weight'] = -15;

  // Add our own validation.
  $form['#validate'][] = '_authbucket_form_install_configure_form_validate';

  // Add our own submit handler.
  $form['#submit'][] = '_authbucket_form_install_configure_form_submit';
}

/**
 * Validate Power user account.
 */
function _authbucket_form_install_configure_form_validate($form, FormStateInterface $form_state) {
  // Check admin account.
  if ($error = user_validate_name($form_state->getValue(['webmaster_account', 'name']))) {
    $form_state->setErrorByName('webmaster_account][name', $error);
  }
  if ($form_state->getValue(['webmaster_account', 'name']) == $form_state->getValue(['account', 'name'])) {
    $form_state->setErrorByName('webmaster_account][name', t('The admin name is not valid.'));
  }
  if ($form_state->getValue(['webmaster_account', 'mail']) == $form_state->getValue(['account', 'mail'])) {
    $form_state->setErrorByName('webmaster_account][mail', t('The admin email address is not valid.'));
  }
}

/**
 * Create Power user account and change root password.
 */
function _authbucket_form_install_configure_form_submit($form, FormStateInterface $form_state) {
  // Add the user and associate role here.
  $role = Role::load('power');
  if (!$role) {
    $role = Role::create([
      'id' => 'power',
      'label' => 'Power user',
    ]);
    $role->save();
  }

  // We keep power user and administrator account password in sync by default.
  $account = User::create([
    'mail' => $form_state->getValue(['webmaster_account', 'mail']),
    'name' => $form_state->getValue(['webmaster_account', 'name']),
    'pass' => $form_state->getValue(['account', 'pass']),
    'init' => $form_state->getValue(['webmaster_account', 'mail']),
    'status' => 1,
    'roles' => [$role],
  ]);
  $account->save();
}

/**
 * Configure non-backward-compatibile modules.
 */
function _authbucket_configure_modules() {
  $modules = [
    'authbucket_core',
  ];

  // Enable modules.
  \Drupal::service('module_installer')->install($modules);

  // Revert features.
  $manager = \Drupal::service('features.manager');
  $config_update = \Drupal::service('features.config_update');
  $module_handler = \Drupal::moduleHandler();
  $config = $manager->getConfigCollection();
  $module_list = $module_handler->getModuleList();
  foreach ($modules as $module) {
    $extension = $module_list[$module];
    $feature = $manager->initPackageFromExtension($extension);
    $feature->setConfig($manager->listExtensionConfig($extension));
    $feature->setStatus(FeaturesManagerInterface::STATUS_INSTALLED);
    $components = $feature->getConfigOrig();
    foreach ($components as $component) {
      if (!isset($config[$component])) {
        $item = $manager->getConfigType($component);
        $type = ConfigurationItem::fromConfigStringToConfigType($item['type']);
        $config_update->import($type, $item['name_short']);
      }
      else {
        $item = $config[$component];
        $type = ConfigurationItem::fromConfigStringToConfigType($item->getType());
        $config_update->revert($type, $item->getShortName());
      }
    }
  }
}

/**
 * Various actions needed to clean up after the installation.
 */
function _authbucket_configure_cleanup() {
  // Clear the APC cache to ensure APC class loader is reset.
  if (function_exists('apc_fetch')) {
    apc_clear_cache('user');
  }

  // Flush all existing path aliases.
  db_delete('url_alias');

  // Rebuild node access database.
  node_access_rebuild();

  // Apply entity schema updates.
  \Drupal::entityDefinitionUpdateManager()->applyUpdates();

  // Rebuild the menu.
  \Drupal::service('router.builder')->rebuild();

  // Clear out caches.
  drupal_flush_all_caches();

  // Clear out JS and CSS caches.
  \Drupal::service('asset.css.collection_optimizer')->deleteAll();
  \Drupal::service('asset.js.collection_optimizer')->deleteAll();

  // Clear drupal message queue for non-errors.
  drupal_get_messages('status', TRUE);
  drupal_get_messages('warning', TRUE);
}
